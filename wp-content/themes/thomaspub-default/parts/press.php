<section class="social-module lightblue-module">
                <div class="inner-wrap ">
                <div class="rows-of-3">
                    <!--<section class="social-module-blog">
                        <h2 class="section-header">Talking Business Blogs</h2>
                         <article class="feed-item">
                            <h3 class="feed-item-header">
                                <span>June 25, 2014</span>
                            </h3>
                            <p class="feed-item-body">
                                <a href="#" class="raquo">Pellentesque acilisis hendrerit elementum hendrerit facilisis ipsum</a>
                            </p>
                        </article>
                        <article class="feed-item">
                            <h3 class="feed-item-header">
                                <span>June 25, 2014</span>
                            </h3>
                            <p class="feed-item-body">
                                <a href="#" class="raquo">Pellentesque acilisis hendrerit elementum hendrerit facilisis ipsum</a>
                            </p>
                        </article>
                        <article class="feed-item">
                            <h3 class="feed-item-header">
                                <span>June 25, 2014</span>
                            </h3>
                            <p class="feed-item-body">
                                <a href="#" class="raquo">Pellentesque acilisis hendrerit elementum hendrerit facilisis ipsum</a>
                            </p>
                        </article>
                         <p class="cta-link-feed"><a href="#" class="raquo cta-link cta-link-large">Visit Blog</a></p>
                    </section>-->
<?php 
// the query
$the_query = new WP_Query(array( 'post_type' => 'press', 'posts_per_page' => 20, )); ?>

<?php if ( $the_query->have_posts() ) : ?>



<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
<section class="press-release-feed">
                      <article class="feed-item">
                            <h3 class="feed-item-header">
                                <span><?php the_time('F j, Y'); ?></span>
                            </h3>
                            <p class="feed-item-body">
                                <a href="<?php if(get_field("pr_url")):  the_field("pr_url"); else: the_permalink(); endif; ?>" class="raquo" <?php if(get_field("pr_url")): ?>target="_blank"<?php endif; ?> ><?php the_title(); ?></a>
                            </p>
                        </article>
                        
                         
                    </section>  

                   

<?php endwhile; ?> 


<?php else : ?>
<p class="emph">Sorry, but we do not have any press releases at the moment. Check back soon.</p>
<?php wp_reset_postdata(); ?>

<?php endif; ?>



                </div>    

        <h3><a href="http://www.thomasnet.com/pressroom/" class="raquo cta-link cta-link-large" target="_blank">View all Press Releases</a></h3>
                   
                </div>
            </section>


            