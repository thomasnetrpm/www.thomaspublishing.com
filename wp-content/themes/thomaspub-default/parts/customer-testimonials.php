<section class="video-module dark-module slant-white-right">
                <div class="inner-wrap">
                    <div class="video-module-body">
                        <h2 class="large-header">
                            See how our customers <br>
                            <span>define success</span>
                        </h2>
                        <p class="large-text ">
                            Greater efficiencies. Better sales inquiries. Stronger brands. Bottom line results. The people we serve measure success in many ways – see what they have to say about how we help them reach their goals. 
                        </p>
                        <p class="video-module-cta">
                            <a href="<?php bloginfo('url'); ?>/client-spotlight" class="raquo cta-link cta-link-large">See our customer testimonials</a>
                        </p>
                    </div>
                    <figure class="video-module-figure">
                    
                        <img src="<?php bloginfo('template_url'); ?>/img/video-still-sales.jpg" alt="Video">
                   
                </figure>
                </div>
                
            </section>