<div class="inner-wrap">
    <div class="row your-peers-grid rows-of-3">
<?php if( have_rows('add_testmonial') ):      
                    while ( have_rows('add_testmonial') ) : the_row();
                    $thumbnail = get_sub_field('cs_thumbnail');
                    $logo = get_sub_field('cs_logo');
                     ?>
<a href="<?php the_sub_field('cs_url'); ?>" class="your-peers-item" target="_blank">
        <div class="your-peers-testimonial">
            <figure class="">
                 <img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>">
            </figure>
        </div>
        <figcaption>
            <b><?php the_sub_field('cs_title'); ?></b>
            <hr class="short-width">
            <i><?php the_sub_field('cs_location'); ?></i>
        </figcaption>
        <figure>
                 <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
        </figure>
        <div class="your-peers-hover-text">
            <p><?php the_sub_field('cs_hovertext'); ?></p>
        </div>
    </a>


<?php endwhile; ?>
                    <?php endif; ?>



    
    </div>
</div>