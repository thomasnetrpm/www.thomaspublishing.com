<?php

	/*
		Template Name: Careers
	*/
?>
 
 <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/page-header' ) ); ?>


	    <div class="inner-wrap-narrow">
	    	<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/page-utility' ) ); ?> 
	        	
	       		<?php the_content(); ?> 
				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
				                    
	        
	    </div>


<?php endwhile; ?>



<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>