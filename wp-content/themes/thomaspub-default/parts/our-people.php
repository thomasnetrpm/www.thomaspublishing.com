<section class="our-people-section">
    <div class="inner-wrap rows-of-3">
    <div class="ops-item-wrap">
        <figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-joe.jpg" alt="">
                        <div class="ops-item-content">
<h2><span>Joe</span></h2>
<p><strong>Title:</strong> Director of Client Services<br>
<strong>Division:</strong> ThomasNet <br>
<strong>At Thomas:</strong> I’ve been here 6 ½ years</p>

<p><strong>What one word best describes your job at Thomas?</strong><br>
Fun.</p>
<p><strong>For you, what makes Thomas a great place to work?</strong><br>
The people and environment at 5 Penn. There has not been one day in my 6 1/2 years at ThomasNet that I did not want to come into work!</p>
<p><strong>Describe, in a few sentences, something exciting you recently worked on.</strong><br>
Two things that are exciting to me are the two new teams we started this year - 
The Application Engineering team to support the selling of media pre sale and the Client Services department headed up by Todd Worms that works with media clients post sale.</p>
<p><strong>How did you find your job at Thomas?</strong><br>
Careerbuilder.com</p>
<p><strong>Name a favorite past time outside of work.</strong><br>
Hanging with my two girls, golf, and cooking.</p>


            </div>
        </figure></div>

        <div class="ops-item-wrap">
        <figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-doug.jpg" alt="">
             <div class="ops-item-content">
<h2><span>Doug </span></h2>

<p><strong>Title:</strong> Director of International Operations<br>
<strong>Division:</strong> Thomas Publishing International <br>
<strong>At Thomas:</strong> I’ve been here 30 years </p>

<p><strong>What one word best describes your job at Thomas?</strong><br>
Ever-changing.</p>
<p><strong>For you, what makes Thomas a great place to work?</strong><br>
Employees at Thomas are encouraged to learn more, and do more – to try new things that bring value to our customers and to Thomas.</p>
<p><strong>Describe, in a few sentences, something exciting you recently worked on.</strong><br>
I recently worked with my colleague to launch a Spanish-language version of our e-newsletter and website, targeting Central and South America. We were able to leverage our low-cost publishing platforms and business connections to move from concept to product in 3 months.</p>
<p><strong>How did you find your job at Thomas?</strong>
I found my job from an ad in the NY Times.</p>
<p><strong>Name a favorite past time outside of work.</strong><br>
Hiking.
</p>


            </div>
        </figure></div>

        <div class="ops-item-wrap">
        <figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-kristin.jpg" alt="">
                         <div class="ops-item-content">

<h2><span>Kristin </span></h2>



<p><strong>Title:</strong> Audience Outreach Manager<br>
<strong>Division:</strong> ThomasNet Audience Development <br>
<strong>At Thomas:</strong> 3 years </p>

<p><strong>What one word best describes your job at Thomas?</strong><br>
Rewarding.
 </p>
<p><strong>For you, what makes Thomas a great place to work?</strong><br>
 
While my core responsibility is working with buyers and engineers to create awareness and usage of theThomasNet.com platform, I am involved in many internal projects. This allows me to interact with lots of other employees and acquire new skills.
</p>
 
<p><strong>Describe, in a few sentences, something exciting you recently worked on.</strong><br>
Last spring, ThomasNet partnered  with ISM, the largest association of Supply Chain professionals, to create a 30 under 30 in Supply Chain Recognition Program.  As part of the core team, I was involved in all aspects of the program -- nomination guidelines, marketing strategy, website development,  winner selection and ceremonies.  The program was extremely successful and as a result we are about to open up nominations for 2015

</p>

<p><strong>How did you find your job at Thomas?</strong><br>
I was actually recruited through LinkedIn.</p> 

<p> <strong>Name a favorite past time outside of work.</strong><br>
Traveling! Which is why I love my job so much.</p>

            </div>
        </figure></div>


       


        <!--<figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-emma.jpg" alt="">
             <div class="ops-item-content">




            </div>
        </figure>-->
         <div class="ops-item-wrap">
             <figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-brittany.jpg" alt="">
            <div class="ops-item-content">
                <h2><span>Brittany</span></h2>


<p><strong>Title:</strong> Social Media and Email Marketing Product Manager<br>
<strong>Division:</strong> ThomasNet RPM<br>
<strong>At Thomas:</strong> I’ve been here 4 years</p>


<p><strong>What one word best describes your job at Thomas?</strong><br>
 Valuable.</p>
<p><strong>For you, what makes Thomas a great place to work?</strong><br>
If you are a rockstar, that will be recognized and followed up with ample opportunities for growth.  I also like how there isn’t (just) one career path for you – there are opportunities for people to grow within their department and outside of their department. Plus, it’s also great to be surrounded by people who work as hard as you do.  We all work really hard together and are always looking to achieve more than the goals we set for our teams.</p>
<p><strong>Describe, in a few sentences, something exciting you recently worked on.</strong><br>
I have been working on transitioning into managing our Content Marketing programs.  In our group we offer a la carte services as well as full RPM programs.  Now we are offering options for clients that meet in the middle of those two types of programs where we are incorporating multiple marketing tactics.
<p><strong>How did you find your job at Thomas?</strong><br>
I found my job through a temp agency.  I started as an SEO intern for the summer a little less than 4 years ago, and was hired full time as a project manager for social media and email marketing when that department was in its infancy.</p>
<p><strong>Name a favorite past time outside of work.</strong><br>
I love playing soccer!  I’ve been playing in an adult league in Hoboken for the past couple of years.</p>




            </div>
        </figure></div>
         
        
       <!--
        <figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-kristin.jpg" alt="">
             <div class="ops-item-content">



            </div>
        </figure>
-->
        <div class="ops-item-wrap">
        <figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-reggie.jpg" alt="">
             <div class="ops-item-content">

<h2><span>Reggie</span></h2>

<p><strong>Title:</strong> Director Sales and Marketing Relations<br>
<strong>Division:</strong> ThomasNet <br>
<strong>At Thomas:</strong> I’ve been here 20 years</p>

<p><strong>What one word best describes your job at Thomas?</strong><br>
Rewarding.</p>
<p><strong>For you, what makes Thomas a great place to work?</strong><br>
There are 2 things that make Thomas a great place for me: 1) the people I work with; 2) I’m not only permitted but encouraged to contribute to the business</p>
<p><strong>Describe, in a few sentences, something exciting you recently worked on.</strong><br>
New Associate Product Learning Sessions – beyond setting up the logistics, being a part of a new sales associate learning process, making sure they have an understanding of how to position ThomasNet for their and our success is cool to be a part of and witness; especially when an associate really gets the importance of ThomasNet and closes their first sale.</p>
<p><strong>How did you find your job at Thomas?</strong><br>
A friend thought I would be a good fit for the ThomasNet division.</p>
<p><strong>Name a favorite past time outside of work.</strong><br>
Swimming.</p>


            </div>
        </figure></div>

<div class="ops-item-wrap">
<figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-julian.jpg" alt="">
             <div class="ops-item-content">

<h2><span>Julian</span></h2>

<p><strong>Title:</strong> Web Designer / Developer<br>
<strong>Division:</strong> ThomasNet RPM<br>
<strong>At Thomas:</strong> I’ve been here 4 years</p>

<p><strong>What one word best describes your job at Thomas?</strong><br>
Growth.</p>
<p><strong>For you, what makes Thomas a great place to work?</strong><br>
You're working with a handful of open-minded folk. If you have an idea, they will listen.</p>
<p><strong>Describe, in a few sentences, something exciting you recently worked on.</strong><br>
I was the main designer/developer on this website. It's great to be given the reigns on a website of such prestige. </p>
<p><strong>How did you find your job at Thomas?</strong><br>
I was an intern in the marketing department at my college. My supervisor left to work for Thomas and gave me the recommendation. I had the job waiting for me before I graduated school.</p>
<p><strong>Name a favorite past time outside of work.</strong><br>
Sports photography.</p>


            </div>
        </figure></div>
        
        <div class="ops-item-wrap">
        <figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-madison.jpg" alt="">
            <div class="ops-item-content">
                <h2><span>Madison</span></h2>

<p><strong>Title:</strong> Enterprise Project Manager<br>
<strong>Division:</strong> Thomas Enterprise <br>
<strong>At Thomas:</strong> I’ve been here 5 years</p>

<p><strong>What one word best describes your job at Thomas?</strong><br>
Projects.</p>
<p><strong>For you, what makes Thomas a great place to work?</strong><br>
My colleagues.  Whether it be those I see on a daily basis or those I communicate with virtually, I have built some great relationships throughout the years. </p>
<p><strong>Describe, in a few sentences, something exciting you recently worked on.</strong><br>
I am in the process of building HIWIN Corporation's new website and navigator program. It includes all of the bells and whistles of modern web technology including a unique responsive design, product configurators, dynamic CAD modeling and multi-language capabilities.</p>
<p><strong>How did you find your job at Thomas?</strong><br>
I interned at Thomas during my last summer of college.</p>
<p><strong>Name a favorite past time outside of work.</strong><br>
Spending time anywhere that has sun and sand.</p>





            </div>
        </figure></div>

        <div class="ops-item-wrap">
         <figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-vito.jpg" alt="">
             <div class="ops-item-content">

<h2><span>Vito</span></h2>

<p><strong>Title:</strong> Junior Business Analyst<br>
<strong>Division:</strong> ThomasNet <br>
<strong>At Thomas:</strong> I’ve been here 4 years</p>

<p><strong>What one word best describes your job at Thomas?</strong><br>
Multi-Tasking.</p>
<p><strong>For you, what makes Thomas a great place to work?</strong><br>
The people are what make Thomas a great place to work.  It's a great working environment and I believe the reason for that is the many people spread throughout the company who are willing to help and assist you on a daily basis.  People at Thomas enjoy helping others and it seems to rub off on everyone.</p>
<p><strong>Describe, in a few sentences, something exciting you recently worked on.</strong><br>
I've been working on an ongoing project with IT that is aiming to add some interactive data to the Sales Dashboard that will assist the Sales Reps, Seniors and Directors tremendously.  It will eventually allow them to manage their book of accounts easier, and will also provide data in real time as opposed to requesting a report which becomes out of date the second after it is pulled.  We've made some great progress of late and we will continue to do so in the weeks and months ahead.   </p>
<p><strong>How did you find your job at Thomas?</strong><br>
Believe it or not, I have to thank my mother for bringing me to Thomasnet.  During the summer after my Junior Year in college, Thomas was looking for a temp through her Temporary Staffing Agency, so that was my first experience here.  I worked as a temp at Thomas on a couple of other occasions before working for a Law Firm for 2 years.  When I heard there was an opening for a Full-Time Position back at Thomas, I jumped on it.  </p>
<p><strong>Name a favorite past time outside of work.</strong><br>
Rooting for the New York Rangers.  I'm hoping for a deep run into June this Spring.  Let's go Blueshirts!
</p>


            </div>
        </figure></div>



         <div class="ops-item-wrap">
         <figure class="ops-item">
            <img src="<?php bloginfo('template_url'); ?>/img/people-joen.jpg" alt="">
             <div class="ops-item-content">


<h2><span>Joseph </span></h2>

<p><strong>Title:</strong> Manager, Business Intelligence &amp; Program Support<br>
<strong>Division:</strong> ThomasNet <br>
<strong>At Thomas:</strong> I’ve been here 6 years</p>

<p><strong>What one word best describes your job at Thomas?</strong><br>
Captivating.</p>
<p><strong>For you, what makes Thomas a great place to work?</strong><br>
The leeway to explore new initiatives that take the company in new directions, and also the open door policy maintained by our company's executive management.</p>
<p><strong>Describe, in a few sentences, something exciting you recently worked on.</strong><br>
Reinventing how program R.O.I. is presented to our client base, through simplification of data and visualization of our industry leading user base. </p>
<p><strong>How did you find your job at Thomas?</strong><br>
A technology consulting and staffing agency originally brought me to Thomas after I decided to leave the finance industry in 2009. My current position did not exist at the time, however, and was created a year after joining the company.</p>
<p><strong>Name a favorite past time outside of work.</strong><br>
Playing classical music on my ukuleles and contributing to a friend's extensive collection of published classical ukulele arrangements. I love to play outdoors, when NYC weather permits!</p>


            </div>
        </figure></div>
    </div>
</section>
