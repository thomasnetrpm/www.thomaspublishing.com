<?php if( get_field('pqm_title')) :?>
   
   
<section class="video-module dark-module slant-white-right">
    <div class="inner-wrap">
        <div class="video-module-body">
            <h2 class="large-header">
               <?php the_field('pqm_title'); ?>
            </h2>
            <p class="large-text ">
                <?php the_field('pqm_body'); ?> 
            </p>
            <p class="video-module-cta">
                <a href="<?php the_field('pqm_url'); ?>" class="raquo cta-link cta-link-large">
                <?php if(get_field('pqm_url_text')) : ?>
                    <?php the_field('pqm_url_text'); ?>
                    
                <?php else : ?>

                    Find out more about <?php the_field('pqm_name'); ?> &amp; others at Thomas 
                
                <?php endif; ?>
                </a>
            </p>
        </div>
        <figure class="video-module-figure" style="background-image:url(<?php the_field('pqm_img'); ?>);">
        
             
       
    </figure>
    </div>
    
</section>


<?php endif; ?>



<?php if (get_field('pqm_title') == false && is_front_page() == false && is_singular( 'job' ) == false && is_page( 1094 ) == false) : ?>
<section class="video-module dark-module slant-white-right">
    <div class="inner-wrap">
        <div class="video-module-body">
            <h2 class="large-header">
               <?php the_field('global_pqm_title','option'); ?>
            </h2>
            <p class="large-text ">
                <?php the_field('global_pqm_body','option'); ?> 
            </p>
            <p class="video-module-cta">
                <a href="<?php the_field('global_pqm_url','option'); ?>" class="raquo cta-link cta-link-large">
                <?php if(get_field('global_pqm_url_text','option')) : ?>
                    <?php the_field('global_pqm_url_text','option'); ?>
                    
                <?php else : ?>

                    Find out more about <?php the_field('global_pqm_name','option'); ?> &amp; others at Thomas 
                
                <?php endif; ?>
                </a>
            </p>
        </div>
        <figure class="video-module-figure" style="background-image:url(<?php the_field('global_pqm_img','option'); ?>);">
        
             
       
    </figure>
    </div>
    
</section>

<?php endif; ?>