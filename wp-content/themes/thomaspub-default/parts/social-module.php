<section class="social-module lightblue-module">
                <div class="inner-wrap rows-of-3">
                    <!--<section class="social-module-blog">
                        <h2 class="section-header">Talking Business Blogs</h2>
                         <article class="feed-item">
                            <h3 class="feed-item-header">
                                <span>June 25, 2014</span>
                            </h3>
                            <p class="feed-item-body">
                                <a href="#" class="raquo">Pellentesque acilisis hendrerit elementum hendrerit facilisis ipsum</a>
                            </p>
                        </article>
                        <article class="feed-item">
                            <h3 class="feed-item-header">
                                <span>June 25, 2014</span>
                            </h3>
                            <p class="feed-item-body">
                                <a href="#" class="raquo">Pellentesque acilisis hendrerit elementum hendrerit facilisis ipsum</a>
                            </p>
                        </article>
                        <article class="feed-item">
                            <h3 class="feed-item-header">
                                <span>June 25, 2014</span>
                            </h3>
                            <p class="feed-item-body">
                                <a href="#" class="raquo">Pellentesque acilisis hendrerit elementum hendrerit facilisis ipsum</a>
                            </p>
                        </article>
                         <p class="cta-link-feed"><a href="#" class="raquo cta-link cta-link-large">Visit Blog</a></p>
                    </section>-->

                    <section class="social-module-feed-twitter">
                       <h3 class="section-header">Tweets</h2>
                         
                         <div id="rss-feeds"></div>
                         <a href="//twitter.com/TPCoLLC/" class="raquo cta-link" target="_blank">View all tweets</a>
                         
                    </section>

                    <section class="social-module-feed-blog">
                        <h3 class="section-header">Posts</h2>
                         <script type="text/javascript" src="http://output13.rssinclude.com/output?type=js&amp;id=993632&amp;hash=3bd9404348e79f1000139e67e96beda3"></script>
                         <a href="//blog.thomasnet.com/" class="raquo cta-link" target="_blank">View all blog posts</a>
                    </section>

                    <section class="social-module-feed-news">
                        <h3 class="section-header">News</h2>
                        <div class="rssincl-content">




                            <?php 
// the query
$the_query = new WP_Query(array( 'post_type' => 'press', 'posts_per_page' => 4, )); ?>

<?php if ( $the_query->have_posts() ) : ?>



<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="rssincl-entry">
                           
                            <p class="rssincl-itemtitle">
                            <a href="<?php the_field("pr_url");  ?>" target="_blank"><?php the_title(); ?></a>
                            </p>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <div class="rssincl-clear"></div>
                            </div>
                            
                   

<?php endwhile; ?> 


<?php else : ?>
<p class="emph">Sorry, but we do not have any press releases at the moment. Check back soon.</p>
<?php wp_reset_postdata(); ?>

<?php endif; ?>

<a href="http://www.thomasnet.com/pressroom/" class="raquo cta-link" target="_blank">View all articles</a>



                            </div>
                         
                    </section>
                </div>
            </section>