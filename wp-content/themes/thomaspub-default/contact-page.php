<?php

	/*
		Template Name: Contact
	*/
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/page-header' ) ); ?>


	    <div class="inner-wrap">

	    <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/page-utility' ) ); ?>
	    
	        	
	       		<?php the_content(); ?> 




<ul class="accordion-tabs-minimal">
  <li class="tab-header-and-content">
    <a href="#" class="tab-link is-active"><h2>Domestic</h2></a>
    <div class="tab-content">

		<?php if( have_rows('domestic_contacts') ): while( have_rows('domestic_contacts') ): the_row(); ?>
		<div class="bds-item">
			<header class="bds-item-header">
				<h3><?php the_sub_field('department_name'); ?></h3>
			</header>
			<div class="bds-item-body">
				<?php the_sub_field('contact_info'); ?>
			</div>
		</div>
		<?php endwhile; endif; ?>

</div>

  </li>
  <li class="tab-header-and-content">
    <a href="#" class="tab-link"><h2>International</h2></a>
    <div class="tab-content">

			<?php if( have_rows('international_contacts') ): while( have_rows('international_contacts') ): the_row(); ?>
			<div class="bds-item">
				<header class="bds-item-header">
					<h3><?php the_sub_field('department_name'); ?></h3>
				</header>
				<div class="bds-item-body">
					<?php the_sub_field('contact_info'); ?>
				</div>
			</div>
			<?php endwhile; endif; ?>

    </div>
  </li>

  <li class="tab-header-and-content">
    <a href="#" class="tab-link"><h2>General Inquiries</h2></a>
    <div class="tab-content">

<section class="lightblue-module ">
	    	<div class="inner-wrap-narrow">
	    	<h2 class="section-header"><span>Contact</span> Form</h2>
	    	<p>Please fill out this form.
Fields with * are required.</p>
	    		<?php echo do_shortcode( '[contact-form-7 id="1145" title="General Contact Form"]' ); ?>
	    	</div>
	    </section>
    </div>
  </li>
  
</ul>






			
				           

	    </div>






	    

<?php endwhile; ?>



<?php Starkers_Utilities::get_template_parts( array( 'parts/working-at-thomas' ) ); ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>