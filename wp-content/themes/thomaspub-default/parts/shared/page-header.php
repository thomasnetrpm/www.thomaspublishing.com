<section class="page-intro slant-white-left">

    <div class="inner-wrap page-intro-wrap">
        <div class="page-intro-text">
         
            <h1 class="page-intro-header">
              


<?php if(is_404()) : ?>
404: Page Not Found

<?php elseif(is_search()) : ?>
	Search Results for '<?php echo get_search_query(); ?>'

<?php else : ?>
<?php
if(get_field('header_h1'))
{
    echo get_field('header_h1');
}
 else 
{
  the_title();
}
?>

<?php endif; ?>
            </h1> 

<?php if ( 'job' == get_post_type() && is_search() == false) : ?>
    <h2 class="page-intro-subheader">
        <?php $terms = get_the_terms( $post->ID, 'job_category' ); if ( $terms && ! is_wp_error( $terms ) ) :                
        $job_categories = array();
        foreach ( $terms as $term ) {
        $job_categories[] = $term->name;
        }
        $job_category = join( ", ", $job_categories );
        ?>
        <?php echo $job_category; ?>  /
        <?php endif; ?>
        <?php $terms = get_the_terms( $post->ID, 'job_location' ); if ( $terms && ! is_wp_error( $terms ) ) :           
        $job_locations = array();
        foreach ( $terms as $term ) {
        $job_locations[] = $term->name;
        }
        $job_location = join( ", ", $job_locations );
        ?>
        <?php echo $job_location; ?>
        <?php endif; ?> 
    </h2>
<?php endif; ?>



           


        </div>
        <!--<blockquote class="site-intro-blockquote">
            Initiatives that help sustain our industry
            <span class="serif raquo">Nominate a Rising Supply Chain Star</span>
        </blockquote>-->
    </div>
</section>


