





<!--Site Header-->
        <header class="site-header" role="banner">
            <div class="site-header-outer-wrap">
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/search-module' ) ); ?>
<div class="site-header-top">
    
<div class="inner-wrap">
        <a href="<?php bloginfo('url'); ?>" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/tpco-logo.jpg" alt="Site Logo"></a>


<!--Site Nav-->
<div class="site-nav-container">
    
      <div class="snc-header">
    <a href="" class="close-menu menu-link">Close</a>
  </div>
   <?php wp_nav_menu(array(
        'menu'            => 'Primary Nav',
        'container'       => 'nav',
        'container_class' => 'site-nav',
        'menu_class'      => 'sn-level-1',
        'walker'        => new themeslug_walker_nav_menu
        )); ?>
  
</div>
<a href="" class="site-nav-container-screen menu-link">&nbsp;</a>

<!--Site Nav END-->

<span class="sh-icons">
        <a class="sh-ico-search search-link" target="_blank" href="#"><span>Search</span></a>
          <a href="#menu" class="sh-ico-menu menu-link"><span>Menu</span></a>
          </span>
</div> <!--inner-wrap END -->
</div> <!--site-header-top END -->

                <div class="utility-nav">
                    <div class="inner-wrap">
                    
                        <div class="social-wrap tablet">
                            <a href="https://www.facebook.com/THOMASNET-97220725556/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-facebook.png" alt="Facebook"></a>
                            <a href="https://twitter.com/ThomasNet" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-twitter.png" alt="Twitter"></a>
                          <!--<a href="#" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-googleplus.png" alt="Google Plus"></a>-->
                            <a href="https://www.linkedin.com/company/thomasnet" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-linkedin.png" alt="LinkedIn"></a>
                            <a href="<?php bloginfo('url'); ?>/contact" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-email.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-email.png" alt="Email"></a>
                        </div>
                        
                    </div>
                </div>
            </div>    
        </header>





        
<!--Site Content-->
<section class="site-content" role="main">