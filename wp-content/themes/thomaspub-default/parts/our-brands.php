<section class="our-brands-section">
    <div class="inner-wrap" id="thomasnet">
        <h2 class="large-header">THOMASNET.com&reg;</h2>
        
        <article class="obs-body">
            
        <p class="emph">
            We connect the right buyers with the right suppliers through industry’s leading supplier discovery platform. 
        </p>
        <p>THOMASNET.com&reg; combines over 100 years of history with powerful technology to connect buyers and suppliers across all industrial sectors. First-hand industry experience and expertise enables us to develop and provide the platform, information, and services that help buyers and suppliers run their businesses more effectively.</p>

        <p>The THOMASNET.com&reg; platform brings buyers and sellers together at the right place, with the right information, at the right time. We combine the expertise of engineers, designers, and user experience specialists to create the most effective tool of its kind and one that is embedded as a valued source for its users.</p>

        <p>A who’s who of purchasing decision makers comprises the THOMASNET.com&reg; audience as they search over 65,000 categories daily. More powerful than a search engine or directory, the platform provides the backbone of a comprehensive sales and marketing service.</p>
        </article>
        <figure class="obs-visual-thomasnet"><span class="img-wrap">
            <img src="<?php bloginfo('template_url'); ?>/img/visual-thomasnet.jpg" alt="ThomasNet">
        </span>
        <figcaption>
            
           
           <a href="http://www.thomasnet.com" target="_blank">Find Suppliers</a><br>
           <a href="http://business.thomasnet.com/free-profile">Get Found by Buyers</a>
        </figcaption>
        </figure>
    </div>
    <hr>

    <div class="inner-wrap" id="thomasnetrpm">
    <h2 class="large-header text-alignright">ThomasNet RPM&trade;</h2>
        <figure class="obs-visual-rpm">
         <span class="img-wrap">
             <img src="<?php bloginfo('template_url'); ?>/img/visual-rpm.jpg" alt="ThomasNet RPM">
         </span>
            <figcaption>
                <a href="http://business.thomasnet.com/rpm" target="_blank">business.thomasnet.com/rpm</a><br>
                
                
            </figcaption>
         </figure>

        <article class="obs-body text-alignright">
             
        <p class="emph">
           We are a full-service digital agency solution that combines leading-edge inbound marketing strategies with our 100+ years of industrial marketing experience.
        </p>
        <p>Industrial experience. Engineering credentials. Technical writing expertise. These are traits you wouldn't expect from your typical marketing partner. And they're exactly why ThomasNet RPM&trade; delivers anything but typical results. Our singular focus for over 100 years has been bringing buyers and suppliers together in the industrial and manufacturing space. We understand your business, your industry, your customers, and what’s important to each of you. And that understanding makes all the difference in the world.</p>
       


        
        </article>
        
    </div>

    <hr>
    <div class="inner-wrap" id="inboundlogistics">
       <h2 class="large-header">Inbound Logistics</h2>
       
        <article class="obs-body">
            <p class="emph">We deliver timely, game-changing information for today’s supply chain and logistics professionals. 
        </p>
        <p>Key decision makers and executives rely on Inbound Logistics to find supply chain solutions and stay aware of issues and innovations within the industry. We serve our audience with independent editorial content as well as information from supply chain management and logistics service providers. </p>

        <p>When Inbound Logistics produced our first issue in 1981, the term “supply chain” didn’t exist. The ideas we advocated—realigning business by better matching supply to demand, and speeding and reducing inventory—were diametrically opposed to what most were practicing. </p>

        <p>By staying true to our educational mission, IL became the change agent for reinventing supply chain processes, and a catalyst for a new approach to business operations. Over the past three decades, we’ve gone from being an outsider with counter-culture ideas of how transportation should fit within the enterprise, to the information leader in supply chain and logistics management.</p>
        </article>
         <figure class="obs-visual-inbound">
       <span class="img-wrap">
           <img src="<?php bloginfo('template_url'); ?>/img/visual-inbound.jpg" alt="Inbound Logistics">
       </span>
            <figcaption><a href="http://www.inboundlogistics.com/cms/index.php" target="_blank">InboundLogistics.com</a></figcaption>
       </figure>
    </div>
    <hr>
    <div class="inner-wrap" id="thomasinternational">
    <h2 class="large-header text-alignright">Thomas International <br>Publishing Company</h2>
       <figure class="obs-visual-international"><span class="img-wrap"><img src="<?php bloginfo('template_url'); ?>/img/visual-international.jpg" alt=""></span>
            <figcaption>
                
                <a href="https://www.thomaspublishing.com/thomas-international-publishing-company">Thomas International Publishing Company</a>
                
            </figcaption>
         </figure>
        <article class="obs-body text-alignright">
             
        <p class="emph">
           We help industrial marketers boost worldwide brand recognition and sales through targeted global publications.
        </p>


<p>Leveraging the channels that today’s worldwide industrial buyers and decision makers use, including e-magazines, mobile apps, and a destination website, Thomas International elevates market recognition and brand awareness.</p>

<p>The World Industrial Reporter is the flagship e-publication of Thomas International, bringing global innovation news to a daily audience of international industrial buyers, managers, and specifiers.  Innovation is a key driver of industrial sourcing and buying across the globe, and is closely associated with positive market and name recognition.</p>

<p>TIPCo also manages a worldwide network of sales agents to support industrial suppliers in creating and distributing highly effective advertising and marketing programs via print, web, email, social and customized messaging. The International Media Group’s representation portfolio includes NEI in Brazil and Product Navi in Japan, with whom TIPCo operates as joint-venture publisher.</p>
    


<!--
<h3>Products Include:</h3>
<p><strong>World Industrial Reporter Website</strong></p>
<p>The World Industrial Reporter Website brings you the latest in industrial innovation. These insights can help keep your business competitive in today’s global marketplace. Industrial innovation covers product introductions, new materials & technologies, plant openings, enhanced manufacturing processes, and much more. These difficult-to-find articles are brought together in one convenient place, so you can quickly assess their impact on your business. You can also download our iOS mobile app to get these informative articles on the go.</p>

<p><strong>World Industrial Reporter Newsletter</strong><br>
<p>The World Industrial Reporter e-newsletter is a great way to catch up on the same industrial innovations and trends posted on our website. Our newsletter is emailed to subscribers twice each month. Subscribe to have our informative e-newsletter delivered directly to your inbox.</p>
  

      
 <p>TIPCo’s International Media Group, through its representation of world-class B2B media in the U.S., Europe, Japan, and Brazil, offers market exposure to more than 500,000 active industrial buyers, managers, and specifiers worldwide. These buying influencers are directly responsible for millions of dollars of product and service purchasing every day.</p>

       <p>The International Media Group’s representation portfolio includes NEI in Brazil and Product Navi in Japan, with whom TIPCo operates as joint-venture publisher.</p>

        <p>TIPCo also manages a worldwide network of sales agents to support industrial suppliers in creating and distributing highly effective advertising and marketing programs via print, web, email, social and customized messaging.</p>

        <h3>Products Include:</h3>
        <p>World Industrial Reporter Website
The World Industrial Reporter Website brings you the latest in industrial innovation. These insights can help keep your business competitive in today’s global marketplace. Industrial innovation covers product introductions, new materials & technologies, plant openings, enhanced manufacturing processes, and much more. These difficult-to-find articles are brought together in one convenient place, so you can quickly assess their impact on your business. You can also download our iOS mobile app to get these informative articles on the go.</p>
        <p>World Industrial Reporter Newsletter
The World Industrial Reporter e-newsletter is a great way to catch up on the same industrial innovations and trends posted on our website. Our newsletter is emailed to subscribers twice each month. Subscribe to have our informative e-newsletter delivered directly to your inbox.</p>-->
       <!-- <p><a href="">Read More +</a></p>-->
        </article>
         
    </div>
    <hr>
    <div class="inner-wrap" id="thomasenterprise">
       <h2 class="large-header">Thomas Enterprise Solutions</h2>
        <article class="obs-body">
        <p class="emph">
            We help manufacturers manage their digital product data and streamline end-to-end business processes. 
        </p>
        <p>Information drives industrial sales, and Thomas Enterprise Solutions provides a suite of services that effectively leverage the valuable website content that helps our clients grow. Global reach and over 100 years of history in the industrial space provide the foundation for an all-encompassing sales and marketing technology platform.</p>

        <p>Our mission is to provide manufacturing companies the technology to effectively deliver a rewarding online experience to their customers while increasing company exposure. By leveraging our innovative Navigator Platform technology, manufacturers can effectively manage their digital product data and streamline end-to-end business processes by seamlessly sharing product content between internal business systems and front-end websites. </p>
        </article>
       <figure class="obs-visual-enterprise"><span class="img-wrap">
           <img src="<?php bloginfo('template_url'); ?>/img/visual-enterprise.jpg" alt="Thomas Enterprise Solutions">
       </span>
            <figcaption><a href="http://www.thomasenterprisesolutions.com/" target="_blank">ThomasEnterpriseSolutions.com</a></figcaption>
       </figure>
       
         
    </div>


    
</section>