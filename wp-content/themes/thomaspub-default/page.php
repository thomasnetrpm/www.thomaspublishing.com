<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/page-header' ) ); ?>


	    <div class="inner-wrap">

	    
	  <div class="page-utility">
			<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<p class="breadcrumbs">','</p>');
			} ?>
	  </div> 
	        	
	       		<?php the_content(); ?> 
				

<?php if (is_page( 'Our People' )) : ?>
	<?php Starkers_Utilities::get_template_parts( array( 'parts/our-people' ) ); ?>
<?php endif; ?>
				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
				           

	    </div>





<?php if (is_page( '9' )) : ?>
<div class="inner-wrap">
	

					<!--Sitemap Page-->
				    <ul>
				    <?php
				    // Add pages you'd like to exclude in the exclude here
				    wp_list_pages(
				    array(
				    'exclude' => '',
				    'title_li' => '',
				    )
				    );
				    ?>
				    </ul>

</div>

<?php endif; ?>

	    

<?php endwhile; ?>



<?php if (is_page( 'Benefits' )) : ?>
	<?php Starkers_Utilities::get_template_parts( array( 'parts/benefits' ) ); ?>
	<?php Starkers_Utilities::get_template_parts( array( 'parts/jobs-feed' ) ); ?>
	

<?php elseif (is_page( 'Our Brands' )) : ?>
	<?php Starkers_Utilities::get_template_parts( array( 'parts/our-brands' ) ); ?>
	<?php Starkers_Utilities::get_template_parts( array( 'parts/working-at-thomas' ) ); ?>


<?php elseif (is_page( 'Client Spotlight' )) : ?>
	<?php Starkers_Utilities::get_template_parts( array( 'parts/client-spotlight' ) ); ?>
	<?php Starkers_Utilities::get_template_parts( array( 'parts/working-at-thomas' ) ); ?>



<?php elseif (is_page( 'Press' )) : ?>
	<?php Starkers_Utilities::get_template_parts( array( 'parts/press' ) ); ?>
	<?php Starkers_Utilities::get_template_parts( array( 'parts/working-at-thomas' ) ); ?>
	
	


<?php else : ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/working-at-thomas' ) ); ?>

	<?php endif; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>