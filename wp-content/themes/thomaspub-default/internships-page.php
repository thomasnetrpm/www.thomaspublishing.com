<?php

	/*
		Template Name: Internships
	*/
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/page-header' ) ); ?>


	    <div class="inner-wrap">

	    
	    	<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/page-utility' ) ); ?> 
	        	
	       		<?php the_content(); ?> 






			
				           

	    </div>



<section class="picture-module slant-white-right" style="background-image:url(<?php the_field('main_img'); ?>);">
    
</section>
        

<!--

        <section class="light-module">
                <div class="inner-wrap-narrow">
                <h2 class="text-aligncenter">Opportunities exist in the following departments</h2>

                </div>
    
<div class="inner-wrap">
    
    <div class="main">
                <ul class="cbp-ig-grid">
                    <li>
                        <a href="#">
                            <span class="cbp-ig-icon cbp-ig-icon-shoe"><img src="<?php bloginfo('template_url'); ?>/img/ico-writing.svg" alt=""></span>
                            <h3 class="cbp-ig-title">Technical Writing</h3>
                            <span class="cbp-ig-category">Apply</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="cbp-ig-icon cbp-ig-icon-ribbon"><img src="<?php bloginfo('template_url'); ?>/img/ico-writing.svg" alt=""></span>
                            <h3 class="cbp-ig-title">Social Media</h3>
                            <span class="cbp-ig-category">Apply</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="cbp-ig-icon cbp-ig-icon-milk"><img src="<?php bloginfo('template_url'); ?>/img/ico-writing.svg" alt=""></span>
                            <h3 class="cbp-ig-title">Digital Marketing</h3>
                            <span class="cbp-ig-category">Apply</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="cbp-ig-icon cbp-ig-icon-whippy"><img src="<?php bloginfo('template_url'); ?>/img/ico-writing.svg" alt=""></span>
                            <h3 class="cbp-ig-title">Human Resources</h3>
                            <span class="cbp-ig-category">Apply</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="cbp-ig-icon cbp-ig-icon-spectacles"><img src="<?php bloginfo('template_url'); ?>/img/ico-writing.svg" alt=""></span>
                            <h3 class="cbp-ig-title">Finance</h3>
                            <span class="cbp-ig-category">Apply</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="cbp-ig-icon cbp-ig-icon-doumbek"><img src="<?php bloginfo('template_url'); ?>/img/ico-writing.svg" alt=""></span>
                            <h3 class="cbp-ig-title">Sales</h3>
                            <span class="cbp-ig-category">Apply</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="cbp-ig-icon cbp-ig-icon-whippy"><img src="<?php bloginfo('template_url'); ?>/img/ico-writing.svg" alt=""></span>
                            <h3 class="cbp-ig-title">Project Management</h3>
                            <span class="cbp-ig-category">Apply</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="cbp-ig-icon cbp-ig-icon-spectacles"><img src="<?php bloginfo('template_url'); ?>/img/ico-writing.svg" alt=""></span>
                            <h3 class="cbp-ig-title">Information Technology</h3>
                            <span class="cbp-ig-category">Apply</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="cbp-ig-icon cbp-ig-icon-doumbek"><img src="<?php bloginfo('template_url'); ?>/img/ico-writing.svg" alt=""></span>
                            <h3 class="cbp-ig-title">Editorial</h3>
                            <span class="cbp-ig-category">Apply</span>
                        </a>
                    </li>
                </ul>
            </div>


</div>
        </section>


-->



            <section class="light-module">
                <div class="inner-wrap-narrow">
                <h2 class="text-aligncenter">Opportunities exist in the following departments</h2>

                </div>
    
<div class="inner-wrap-narrow">
    
    <div class="circle-links-wrap">
    <a href="javascript:void(0)" class="circle-links">
       <p><span>Marketing</span></p> 
    </a>
    <a href="javascript:void(0)" class="circle-links">
        <p><span>Production</span></p>
    </a>
    <a href="javascript:void(0)" class="circle-links">
        <p><span>Engineering</span></p>
    </a>
    <a href="javascript:void(0)" class="circle-links">
        <p><span>Sales</span></p>
    </a>
    <a href="javascript:void(0)" class="circle-links">
        <p><span>International Business</span></p>
    </a>
    <a href="javascript:void(0)" class="circle-links">
        <p><span>Finance</span></p>
    </a>
    
    
</div>
</div>
        </section>


<section class="dark-module slant-gray-right">
                <div class="inner-wrap">
                    <h2 class="large-header">
                           Do You <span> Qualify?</span>
                        </h2>
                        <p class="large-text ">
                          You must be a student entering your junior or senior year of college. And, you must be able to work legally in the US, possess clear written and oral communication skills, and show an enthusiastic customer service attitude. This is a paid internship with the possibility to gain college credits. 
                          </p>
                        
                </div>
                
            </section>
 




	    

<?php endwhile; ?>



<section class="job-feed-horizontal lightblue-module">
 

<div class="inner-wrap">
<h2 class="section-header">Open Internships</h2>
    <?php 
// the query
$the_query = new WP_Query(array( 'post_type' => 'job', 'posts_per_page' => 2, 

'tax_query' => array(
		array(
			'taxonomy' => 'job_category',
			'field'    => 'slug',
			'terms'    => 'internship',
		),
	),

 )); ?>

<?php if ( $the_query->have_posts() ) : ?>


<div class="rows-of-2">
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

     <article class="jf-item">
        <header class="jf-item-header">

        <h2>
        <?php the_title(); ?></h2>

        <h3><span><?php if( has_term( '', 'job_category' ) )  : ?> 
        <?php $tax_terms = get_the_terms( $post->ID, 'job_category'); foreach ( $tax_terms as $tax_term ) { echo $tax_term->name; }; ?>
        /<?php endif; ?> <?php if( has_term( '', 'job_location' ) )  : ?> 
        <?php $tax_terms = get_the_terms( $post->ID, 'job_location'); foreach ( $tax_terms as $tax_term ) { echo $tax_term->name; }; ?>
        <?php endif; ?></span>
        </h3>
        </header>

        <div class="jf-item-body">
        <?php the_excerpt(); ?>
        </div>

        <div class="jf-item-apply">
        <p><a href="<?php echo get_permalink(); ?>" class="btn">View Job</a> <a href="<?php echo get_permalink(); ?>/#apply" class="btn-important">Apply</a></p>

        </div>
        </article>


      

<?php endwhile; ?> 
</div>

<?php else : ?>
<p class="emph">Sorry, but we do not have any open internships at the moment. Check back soon.</p>
<?php wp_reset_postdata(); ?>

<?php endif; ?>

</div>


</section>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>