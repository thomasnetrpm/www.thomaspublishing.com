<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts() 
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>







<section class="page-intro slant-white-left">
    <div class="inner-wrap page-intro-wrap">
        <div class="page-intro-text">
            <h1 class="page-intro-header">

                <?php if ( 'job' == get_post_type() ) : ?>
                    <?php single_cat_title(); ?> Jobs
                   
                <?php endif; ?>
            </h1>      
        </div>
    </div>
</section>
<header class="header-tabs-section">
        <div class="inner-wrap">
              
      <div class="page-utility">
            <?php if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('<p class="breadcrumbs">','</p>');
            } ?>
      </div> 
               
                    <ul class="header-tabs">
                       
                   <?php wp_nav_menu(array('menu' => 'Jobs Nav','container' => '','items_wrap' => '%3$s',)); ?>
                   </ul>

               
                
        </div>

            </header>   


<section class="job-feed-v2">
	<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <section class="jf-section">
        <div class="inner-wrap">
        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/job-body' ) ); ?>


        </div>
        </section>
    <?php endwhile; ?> 
    <?php wp_reset_postdata(); ?>
    
    <?php else : ?>
    <div class="inner-wrap">
	    <h2>Sorry, no job openings at the moment.</h2>
	    
    </div>
    <?php endif; ?>
</section>


	
	</div>
</section>


<?php Starkers_Utilities::get_template_parts( array( 'parts/working-at-thomas' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>