<section class="jobs-module">
               <div class="inner-wrap">
                    <section class="jobs-module-video">
                        <h2 class="section-header">
                            Working at Thomas
                        </h2>
                        <figure>
                            <a href="https://thomasnet-2.wistia.com/medias/av77b4iyj9" class="wistia-popover" target="_blank">
                                <img src="<?php bloginfo('template_url'); ?>/img/Video_Holder.jpg" alt="Working at Thomas">
                            </a>
                            <!-- <a href="<?php bloginfo('url'); ?>/our-people">
                                <img src="<?php bloginfo('template_url'); ?>/img/home-our-people.jpg" alt="Working at Thomas">
                            </a>-->
                            <figcaption class="figcaption-style"><a href="<?php bloginfo('url'); ?>/our-people" class="raquo cta-link">Meet our People</a></figcaption>
                        </figure>
                    </section>
                    <section class="jobs-module-feed job-feed-short">
                        <h2 class="section-header">
                            Jobs at Thomas
                        </h2>
                        

<?php 
// the query
$the_query = new WP_Query(array( 'post_type' => 'job', 'posts_per_page' => 2,'orderby'=>'menu_order','order'=>'ASC' )); ?>
<?php if ( $the_query->have_posts() ) : ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

      <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/job-body' ) ); ?>

<?php endwhile; ?> 


<?php wp_reset_postdata(); ?>

<?php endif; ?>
                        <p class="cta-link-feed"><a href="<?php bloginfo('url'); ?>/jobs" class="raquo cta-link ">View all job openings</a></p>
                    </section>
               </div>
            </section>
          