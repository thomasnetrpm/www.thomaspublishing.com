

</section> 
<!--end .site-content -->


<!--Site Footer-->
<footer class="site-footer" role="contentinfo">
            <div class="inner-wrap site-footer-wrap">
                  <?php wp_nav_menu(array(
        'menu'            => 'Footer Nav',
        'container'       => 'nav',
        'container_class' => 'site-footer-nav',
        
        )); ?>
                
       <?php the_field('footer_info', 'option'); ?> 

                <div class="site-footer-small">
               
                    <p class="site-footer-copyright">
                        &copy; <?php echo date("Y"); ?> Thomas Publishing Company
                    </p>
                    <p class="site-footer-privacy">
                        <a href="<?php bloginfo('url'); ?>/sitemap">Sitemap</a> | <a href="<?php bloginfo('url'); ?>/privacy">Privacy Policy</a>
                    </p>
               
            </div>
            </div>
            
        </footer>