<section class="light-module">
	<div class="inner-wrap nopadding-top">
		<div class="img-text-item">
			<figure class="iti-figure">
				<img src="" alt="">
			</figure>
			<h2 class="iti-text">
				<span>Health &amp; Wellness</span>
			</h2>

<div class="iti-subcontent rows-of-3">
<div class="iti-subcontent-item">
<h3 class="iti-si-header">Medical Insurance</h3>
<div class="iti-si-content"><p>Effective on the first of the month after the hire date, full time employees, may participate in our Health insurance program offered through Cigna.  The plan allows employees to visit a physician within the network or see a physician of their own choosing.  The plan pays a greater benefit for visits to participating providers.  Employees pay an exceptionally low contribution rate toward the healthcare premium.  </p></div>
</div>
<div class="iti-subcontent-item">
<h3 class="iti-si-header">Dental Insurance</h3>
<div class="iti-si-content"><p>Effective on the first day of the month following the start date, our insurance dental plan offered through Cigna provides coverage for preventive, basic and major dental care expenses.</p></div>
</div>
<div class="iti-subcontent-item">
<h3 class="iti-si-header">Prescription Coverage</h3>
<div class="iti-si-content"><p>Enrollment in the medical plan automatically includes three-tiered prescription drug coverage. With this plan, you pay less for generic drugs than brand-name drugs; mail order maintenance medications have a cost advantage with our plan.</p></div>
</div>

<div class="iti-subcontent-item">
<h3 class="iti-si-header">Health Savings Account</h3>
<div class="iti-si-content"><p>A tax advantaged account provides coverage for current health care expenses with the option to save for future health care expenses.
</p></div>
</div>

<div class="iti-subcontent-item">
<h3 class="iti-si-header"> Supplemental Cancer Care</h3>
<div class="iti-si-content"><p>A voluntary insurance program for employees and their families that provides
                                supplemental Cancer Care insurance through AFLAC®.</p></div>
</div>


<div class="iti-subcontent-item">
<h3 class="iti-si-header">Life &amp; AD&amp;D Insurance</h3>
<div class="iti-si-content"><p>Thomas provides, at no cost to you, life insurance coverage of one times
                                your annual base salary (up to a maximum of $170,000). Free Will Preparation
                                services are offered by Cigna, our insurance company.</p></div>
</div>


<div class="iti-subcontent-item">
<h3 class="iti-si-header">Supplemental Life Insurance</h3>
<div class="iti-si-content"><p>Employees are eligible to apply for supplemental life insurance, aside
                                from the company group life insurance policy. You can apply for additional
                                life insurance coverage to a maximum of $500,000 for yourself; $250,000
                                for your spouse and $5,000 for dependent children.</p></div>
</div>


<div class="iti-subcontent-item">
<h3 class="iti-si-header">Travel Insurance</h3>
<div class="iti-si-content"><p>Issued by one of the leading travel assistance providers in the world,
                                Medex, our free travel insurance, offers services from helping with lost
                                luggage and lost travel documents to preparing for emergency medical evacuations.</p></div>
</div>
<div class="iti-subcontent-item">
<h3 class="iti-si-header">Long Term Disability (LTD)</h3>
<div class="iti-si-content"><p>At no cost to you, the LTD benefit provides income replacement benefit which begins when Short Term Disability ends.  Available until normal retirement age; certain exclusions and limitations apply. </p></div>
</div>
<div class="iti-subcontent-item">
<h3 class="iti-si-header">Voluntary Long Term Disability </h3>
<div class="iti-si-content"><p>Employees earning above certain salary level may be eligible to enroll in this benefit, which is offered through Mass Mutual Insurance Company. </p></div>
</div>
</div>


		</div>
		<div class="img-text-item">
			<figure class="iti-figure">
				
			</figure>
			<h2 class="iti-text">
				<span>Savings & Retirement</span>
			</h2>
<div class="iti-subcontent rows-of-3">
<div class="iti-subcontent-item">
<h3 class="iti-si-header">Profit Sharing &amp; 401(k)</h3>
<div class="iti-si-content"><p>Our profit sharing plan lets employees share in the company's growth and
                                success while they build financial security for the future. Employees can
                                take advantage of pre-tax savings for retirement through our 401(k) plan,
                                which features over 25 funds to choose from.</p></div>
</div>
<div class="iti-subcontent-item">
<h3 class="iti-si-header">Commuter Transit Savings</h3>
<div class="iti-si-content"><p>By enrolling in the commuter transit program you can save money on your
                                commute. It doesn't matter whether you take the subway, bus, train, ferry
                                or even drive to work.</p></div>
</div>
<div class="iti-subcontent-item">
<h3 class="iti-si-header">Flexible Spending Accounts</h3>
<div class="iti-si-content"><p>These accounts help lower your taxable income by paying for health care
                                expenses and dependent care expenses using pre-tax dollars.</p></div>
</div>






</div>


		</div>
		<div class="img-text-item">
			<figure class="iti-figure">
				
			</figure>
			<h2 class="iti-text">
				<span>Work/Life Balance</span>
			</h2>

<div class="iti-subcontent rows-of-3">
<div class="iti-subcontent-item">
<h3 class="iti-si-header">Paid Time Off</h3>
<div class="iti-si-content"><p> Paid time off (PTO) days are in addition to company paid holidays. The
                                generous PTO bank allows you to take time off for personal, vacation and
                                occasional sick time.</p></div>
</div>
<div class="iti-subcontent-item">
<h3 class="iti-si-header"> Bereavement Leave</h3>
<div class="iti-si-content"><p> In the unfortunate event of a death in the family, employees can take
                                up to three paid days for bereavement.</p></div>
</div>
<div class="iti-subcontent-item">
<h3 class="iti-si-header">Paid Holidays</h3>
<div class="iti-si-content">
<p>The company observes eight paid holidays:</p>
<p>
	 New Year's Day, President's Day, Memorial Day, Independence Day, Labor Day, Thanksgiving Day, Christmas Eve & Christmas Day.
</p>
</div>
</div>

<div class="iti-subcontent-item">
<h3 class="iti-si-header">Employee Assistance Program</h3>
<div class="iti-si-content"><p>Thomas recognizes that at times employees may face difficult challenges.
                                Our EAP is a free, confidential, third-party program designed to help employees
                                and family members with a wide variety of issues, including financial concerns,
                                alcohol or drug problems, marital difficulties, illness or emotional stress.</p></div>
</div>


<div class="iti-subcontent-item">
<h3 class="iti-si-header">Health Advocate</h3>
<div class="iti-si-content"><p>Free for employees and their families, Health Advocate complements your
                                basic health insurance coverage by facilitating interactions with healthcare
                                providers and insurers. They help you find solutions to difficult healthcare
                                and insurance problems.</p></div>
</div>


<div class="iti-subcontent-item">
<h3 class="iti-si-header">Thomas University</h3>
<div class="iti-si-content"><p> This internal training program is open to all employees for professional
                                and personal development.</p></div>
</div>


<div class="iti-subcontent-item">
<h3 class="iti-si-header">Employee Discounts</h3>
<div class="iti-si-content"><p>Fantastic discounts on gym memberships, Broadway shows, wireless cell
                                plans and even flowers, just to name a few.</p></div>
</div>

<div class="iti-subcontent-item">
<h3 class="iti-si-header"> Referral Award</h3>
<div class="iti-si-content"><p>Refer a friend to work at Thomas and you can receive a generous reward
                                if the person is hired.</p></div>
</div>

<div class="iti-subcontent-item">
<h3 class="iti-si-header">Easy Friday</h3>
<div class="iti-si-content"><p>Between Memorial and Labor Day all eligible employees are able to take 4 Friday half days to allow you to get a jump start on weekend plans.</p></div>
</div>

</div>



		</div>
	</div>
</section>


  