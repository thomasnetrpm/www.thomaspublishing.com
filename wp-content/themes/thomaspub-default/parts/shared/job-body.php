<article class="jf-item">
    <header class="jf-item-header">

    <h2>
    <?php the_title(); ?></h2>

    <h3><span><?php $terms = get_the_terms( $post->ID, 'job_category' ); if ( $terms && ! is_wp_error( $terms ) ) :                
        $job_categories = array();
        foreach ( $terms as $term ) {
        $job_categories[] = $term->name;
        }
        $job_category = join( ", ", $job_categories );
        ?>
        <?php echo $job_category; ?>  /
        <?php endif; ?>
        <?php $terms = get_the_terms( $post->ID, 'job_location' ); if ( $terms && ! is_wp_error( $terms ) ) :           
        $job_locations = array();
        foreach ( $terms as $term ) {
        $job_locations[] = $term->name;
        }
        $job_location = join( ", ", $job_locations );
        ?>
        <?php echo $job_location; ?>
        <?php endif; ?> </span>
    </h3>
    </header>

    <div class="jf-item-body">
    <?php the_excerpt(); ?>
    </div>

    <div class="jf-item-apply">
    <p><a href="<?php echo get_permalink(); ?>" class="btn">View Job</a> <a href="<?php echo get_permalink(); ?>/#apply" class="btn-important">Apply</a></p>

    </div>
</article>