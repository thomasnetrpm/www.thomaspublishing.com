<section class="site-intro">
<video autoplay  id="bgvid" loop>
<!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
<source src="<?php bloginfo('template_url'); ?>/img/thomas-site-intro.webm" type="video/webm">
   <source src="<?php bloginfo('template_url'); ?>/img/thomas-site-intro.mp4" type="video/mp4">
    

</video>
                <div class="inner-wrap site-intro-wrap">
                    <div class="site-intro-text">
                        <h1 class="site-intro-header">
                            Whatever your role in manufacturing<br>
                            <span class="serif">our focus is on your success</span>
                        </h1>
                        <p class="site-intro-subtext">
                            Let us help build your business.<br>
                            <b>What are you looking for?</b>
                        </p>
                        
                        <a href="" class="btn-blue arrow-down site-intro-cta"><span>I want to...</span></a>
                        
                        <ul class="site-intro-menu">
                            <li class="sim-li-l1">
                                <h3 class="sim-title">Careers</h3>
                                <ul>
                                    <li><a href="<?php bloginfo('url'); ?>/jobs">See career opportunities</a></li>
                                </ul>
                                 <h3 class="sim-title">Supplier Discovery</h3>
                                <ul>
                                    <li><a href="http://www.thomasnet.com/suppliers/" target="_blank">Find qualified suppliers</a></li>
                                    <li><a href="http://productsourcing.thomasnet.com/" target="_blank">Source products</a></li>
                                    <li><a href="http://customquotes.thomasnet.com/" target="_blank">Find a job shop</a></li>
                                </ul>
                            </li>
                            
                            <li class="sim-li-l1">
                                <h3 class="sim-title">Data Management</h3>
                                <ul>
                                    <li><a href="http://www.thomasenterprisesolutions.com/navigator" target="_blank">Centralize/manage my product information</a></li>
                                    <li><a href="http://www.thomasenterprisesolutions.com/configurator" target="_blank">Manage/publish my CAD drawings</a></li>
                                    <li><a href="http://www.thomasenterprisesolutions.com/syndication" target="_blank">Syndicate my data</a></li>
                                    <li><a href="http://www.thomasenterprisesolutions.com/bim" target="_blank">Make my data BIM ready</a></li>
                                </ul>
                            </li>
                            <li class="sim-li-l1">
                                <h3 class="sim-title">News & Information</h3>
                                <ul>
                                    <li><a href="http://news.thomasnet.com/" target="_blank">See the latest supplier innovations </a></li>
                                    <li><a href="http://www.inboundlogistics.com/cms/planner/" target="_blank">View the 2015 Supply Chain/Logistics Planner</a></li>
                                    <li><a href="http://www.inboundlogistics.com/" target="_blank">Read news on inbound logistics</a></li>
                                </ul>
                            </li>
                            <li class="sim-li-l1">
                                <h3 class="sim-title">Marketing/Sales</h3>
                                <ul>
                                    <li><a href="<?php bloginfo('url'); ?>/brands/#thomasinternational" target="_blank">Reach a global market</a></li>
                                    <li><a href="http://business.thomasnet.com/rpm/your-message/website-design" target="_blank">Improve my website</a></li>
                                    <li><a href="http://business.thomasnet.com/rpm/your-message/seo" target="_blank">Rank higher in search engine results</a></li>
                                    <li><a href="http://business.thomasnet.com/rpm/your-message/social-media-manufacturing" target="_blank">Establish a social media program</a></li>
                                </ul>
                            </li>
                        </ul>
                       
                            
                        
                    </div>
                    <!--<blockquote class="site-intro-blockquote">
                        Initiatives that help sustain our industry
                        <span class="serif raquo">Nominate a Rising Supply Chain Star</span>
                    </blockquote>-->
                   
                </div>
                 <section class="site-intro-secondary">
                    <div class="inner-wrap">
                        <div class="site-intro-secondary-blurb">
                            Our brands exist to serve professionals within every <br>
aspect of the industrial buying and selling process. 
                        </div>
                        <!--
                        <div class="site-intro-secondary-body">
                            <img src="<?php bloginfo('template_url'); ?>/img/site-intro-secondary-logos.jpg" alt="">
                        </div>
                        -->
                        <div class="site-intro-logos">
                            <a href="<?php bloginfo('url'); ?>/brands/#thomasnet" class="sil-logo">
                                <figure><img src="<?php bloginfo('template_url'); ?>/img/logo-thomasnet-new.png">
                                    <figcaption>
                                        We connect the right buyers with the right suppliers through industry’s leading supplier discovery platform. 
                                    </figcaption>
                                </figure>
                            </a>
							 <a href="<?php bloginfo('url'); ?>/brands/#thomasnetrpm" class="sil-logo">
                                <figure><img src="<?php bloginfo('template_url'); ?>/img/logo-rpm.png">
                                    <figcaption>
                                        Full-service digital agency solution that combines leading-edge inbound marketing strategies with 100+ years of industrial marketing experience.
                                    </figcaption>
                                </figure>
                            </a>
                            <a href="<?php bloginfo('url'); ?>/brands/#inboundlogistics" class="sil-logo">
                                <figure><img src="<?php bloginfo('template_url'); ?>/img/logo-inbound.png">
                                    <figcaption>
                                        We deliver timely, game-changing information for today’s supply chain and logistics professionals.
                                    </figcaption>
                                </figure>
                            </a>
                            
                            <a href="<?php bloginfo('url'); ?>/brands/#thomasinternational" class="sil-logo">
                                <figure><img src="<?php bloginfo('template_url'); ?>/img/logo-international.png">
                                    <figcaption>
                                       We help industrial marketers boost worldwide brand recognition and sales through targeted global publications. 
                                    </figcaption>
                                </figure>
                            </a>
                            <a href="<?php bloginfo('url'); ?>/brands/#thomasenterprise" class="sil-logo">
                                <figure><img src="<?php bloginfo('template_url'); ?>/img/logo-enterprise.png">
                                    <figcaption>
                                        We help manufacturers manage their digital product data and streamline end-to-end business processes. 
                                    </figcaption>
                                </figure>
                            </a>
                           
                        </div>
                    </div>
                </section>
                
            </section>