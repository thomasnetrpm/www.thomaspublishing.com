<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts() 
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php 
// the query
$the_query = new WP_Query(array( 'post_type' => 'job', 'nopaging' => -1,'orderby'=>'menu_order','order'=>'ASC', )); ?>

<?php if ( $the_query->have_posts() ) : ?>



<section class="page-intro slant-white-left">
    <div class="inner-wrap page-intro-wrap">
        <div class="page-intro-text">
            <h1 class="page-intro-header">
                <?php if ( 'job' == get_post_type() ) : ?>
                    All Jobs
                    <?php elseif ( is_day() ) : ?>
                    Archive: <?php echo  get_the_date( 'D M Y' ); ?>                       
                    <?php elseif ( is_month() ) : ?>
                    Archive: <?php echo  get_the_date( 'M Y' ); ?>
                    <?php elseif ( is_year() ) : ?>
                    Archive: <?php echo  get_the_date( 'Y' ); ?>                              
                    <?php else : ?>
                    Archive</h1>    
                <?php endif; ?>
            </h1>      
        </div>
        <!--<blockquote class="site-intro-blockquote">
            Initiatives that help sustain our industry
            <span class="serif raquo">Nominate a Rising Supply Chain Star</span>
        </blockquote>-->
    </div>
</section>

<header class="header-tabs-section">
        <div class="inner-wrap">
              
      <div class="page-utility">
            <?php if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('<p class="breadcrumbs">','</p>');
            } ?>
      </div> 

                
                 <ul class="header-tabs">
                       
                   <?php wp_nav_menu(array('menu' => 'Jobs Nav','container' => '','items_wrap' => '%3$s',)); ?>
                   </ul>
                
        </div>

            </header>   


<section class="job-feed-v2">

    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <section class="jf-section">
        <div class="inner-wrap">
        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/job-body' ) ); ?>

        </div>
        </section>
    <?php endwhile; ?> 
    <?php wp_reset_postdata(); ?>
</section>


	
	</div>
</section>

<?php endif; ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/working-at-thomas' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>