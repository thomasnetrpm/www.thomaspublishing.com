<!--Site Search -->
    <div class="search-module">
      <div class="inner-wrap">
        <a href="#" target="_blank" class="search-link search-exit active"><img src="<?php bloginfo('template_url'); ?>/img/ico-exit.svg" alt="Exit"></a>
        <form action="<?php bloginfo('url'); ?>/" method="get" class="search-form">
          <div class="search-table">
            <div class="search-row">
              <div class="search-cell1">
                <input type="text" id="search-site" value="" placeholder="Search Website..." name="s" class="search-text" title="Search Our Site">
              </div>
              <div class="search-cell2">
                <input class="search-submit" alt="Search" title="Search" value="" type="submit">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>