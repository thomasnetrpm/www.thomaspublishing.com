<?php

	/*
		Template Name: Front Page
	*/
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>


<?php Starkers_Utilities::get_template_parts( array( 'parts/site-intro' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/jobs-module' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/customer-testimonials' ) ); ?>


<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>