

<section class="job-feed-horizontal lightblue-module slant-white-right">
 

<div class="inner-wrap">
 <h2 class="section-header">
    Featured Jobs
</h2>
    <?php 
// the query
$the_query = new WP_Query(array( 'post_type' => 'job', 'posts_per_page' => 2,'orderby'=>'menu_order','order'=>'ASC' )); ?>

<?php if ( $the_query->have_posts() ) : ?>


<div class="rows-of-2">
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

     <article class="jf-item">
        <header class="jf-item-header">

        <h2>
        <?php the_title(); ?></h2>

        <h3><span><?php if( has_term( '', 'job_category' ) )  : ?> 
        <?php $tax_terms = get_the_terms( $post->ID, 'job_category'); foreach ( $tax_terms as $tax_term ) { echo $tax_term->name; }; ?>
        /<?php endif; ?> <?php if( has_term( '', 'job_location' ) )  : ?> 
        <?php $tax_terms = get_the_terms( $post->ID, 'job_location'); foreach ( $tax_terms as $tax_term ) { echo $tax_term->name; }; ?>
        <?php endif; ?></span>
        </h3>
        </header>

        <div class="jf-item-body">
        <?php the_excerpt(); ?>
        </div>

        <div class="jf-item-apply">
        <p><a href="<?php echo get_permalink(); ?>" class="btn">View Job</a> <a href="<?php echo get_permalink(); ?>/#apply" class="btn-important">Apply</a></p>

        </div>
        </article>


      

<?php endwhile; ?> 
</div>

<?php else : ?>
<p class="emph">Sorry, but we do not have any open jobs at the moment. Check back soon.</p>
<?php wp_reset_postdata(); ?>

<?php endif; ?>


</section>